; ----------------
; Mark Davies Platform Makefile 
; Permanent URL: https://bitbucket.org/cpmadmin/mark-davies-platform.git
; Build Version: v0.1
; Build Name: build_0_1
; Build Description: Initial Repack of the demo install
; Filename: markdavies.make
; ----------------  

; ------------  
; Core version
; ------------
; Each makefile should begin by declaring the core version of Drupal that all
; projects should be compatible with.
  
core = 7.x
api = 2

; ------------      
; Modules
; ------------

projects[ctools][subdir] = "contrib"
projects[ctools]][version] = "1.2"
projects[ctools]][type] = "module"

projects[features][version] = "1.0"
projects[features][subdir] = "contrib"
projects[features]][type] = "module"

projects[ts_news_features][version] = "1.0"
projects[ts_news_features][subdir] = "contrib"
projects[ts_news_features][type] = "module"

projects[video_embed_field][version] = "2.0-beta5"
projects[video_embed_field][subdir] = "contrib"
projects[video_embed_field][type] = "module"

projects[addthis][version] = "2.1-beta1"
projects[addthis][subdir] = "contrib"
projects[addthis][type] = "module"

projects[libraries][version] = "2.0"
projects[libraries][subdir] = "contrib"
projects[libraries][type] = "module"

projects[menu_block][version] = "2.3"
projects[menu_block][subdir] = "contrib"
projects[menu_block][type] = "module"

projects[menu_position][version] = "1.1"
projects[menu_position][subdir] = "contrib"
projects[menu_position][type] = "module"

projects[pathauto][version] = "1.2"
projects[pathauto][subdir] = "contrib"
projects[pathauto][type] = "module"

projects[quicktabs][version] = "3.4"
projects[quicktabs][subdir] = "contrib"
projects[quicktabs][type] = "module"

projects[robotstxt][version] = "1.1"
projects[robotstxt][subdir] = "contrib"
projects[robotstxt][type] = "module"

projects[taxonomy_breadcrumb][version] = "1.x-dev"
projects[taxonomy_breadcrumb][subdir] = "contrib"
projects[taxonomy_breadcrumb][type] = "module"

projects[token][version] = "1.4"
projects[token][subdir] = "contrib"
projects[token][type] = "module"

projects[entitycache][version] = "1.x-dev"
projects[entitycache][subdir] = "contrib"
projects[entitycache][type] = "module"

projects[taxonomy_menu][version] = "1.4"
projects[taxonomy_menu][subdir] = "contrib"
projects[taxonomy_menu][type] = "module"

projects[wysiwyg][version] = "2.2"
projects[wysiwyg][subdir] = "contrib"
projects[wysiwyg][type] = "module"

projects[views][version] = "3.5"
projects[views][subdir] = "contrib"
projects[views][type] = "module"

projects[views_slideshow][version] = "3.0"
projects[views_slideshow][subdir] = "contrib"
projects[views_slideshow][type] = "module"

; ------------
; Libraries
; ------------

; ------------
; ckeditor
; ------------
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][type] = "library"
libraries[ckeditor][destination] = "libraries"
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.6.1/ckeditor_3.6.6.1.tar.gz"

; ------------
; jquery.cycle
; ------------
libraries[jquery.cycle][directory_name] = "jquery.cycle"
libraries[jquery.cycle][type] = "library"
libraries[jquery.cycle][destination] = "libraries"
libraries[jquery.cycle][download][type] = "get"
libraries[jquery.cycle][download][url] = "http://malsup.github.com/jquery.cycle.all.js"
